FROM node:18-alpine
WORKDIR /docktest-2
COPY . .
RUN yarn install --production
CMD ["node", "./src/index.js"]
